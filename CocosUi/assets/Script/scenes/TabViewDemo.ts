import {TabView} from "../components/common/TabView";

const {ccclass, property} = cc._decorator;
/**
 * 多页标签控制演示.
 */
@ccclass
export default class TabViewDemo extends cc.Component {

    @property(TabView)
    private tabView: TabView = null;

    protected start(): void {
    }

    public changeTab() {
        const randomIndex = Math.floor(Math.random() * this.tabView.pageCount);
        this.tabView.setCurrent(randomIndex);
    }
}