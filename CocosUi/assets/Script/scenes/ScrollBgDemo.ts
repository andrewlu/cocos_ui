/**
 * 材质系统实现的背景滚动效果.
 */

import ccclass = cc._decorator.ccclass;
import property = cc._decorator.property;
import BgMoveComp from "../components/BgMoveComp";

@ccclass()
export class CustomTabPage extends cc.Component {

    @property(BgMoveComp)
    private bgMoveComp: BgMoveComp = null;

    onBgScrollSpeedChange(view:cc.Slider){
        this.bgMoveComp.setSpeed(view.progress * 1000);
    }
}