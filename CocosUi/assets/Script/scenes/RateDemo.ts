import {RateComponent} from "../components/common/RateComponent";

const {ccclass, property} = cc._decorator;
/**
 * 评分星级控制.
 */
@ccclass
export default class TabViewDemo extends cc.Component {

    @property(RateComponent)
    private rateComp: RateComponent = null;

    protected start(): void {
    }

    onRateValueChange(view: cc.Slider) {
        this.rateComp.rate = Math.round(10 * view.progress);
    }

    onRateSpanChange(view: cc.Slider) {
        this.rateComp.span = (view.progress - 0.5) * 2
    }
}