/**
 * 自定义每一tab页的控制器组件.
 */

import ccclass = cc._decorator.ccclass;
import TabViewPage from "./components/common/TabView";

@ccclass()
export class CustomTabPage extends TabViewPage {

    protected onEnable(): void {
        const pageIndex = this.getPageIndex();
        cc.log("当前page页索引:", pageIndex);
    }

    // 想要切换效果就自己实现.
    onPageShowing(): Promise<any> {
        return super.onPageShowing();
    }
    onPageHiding(): Promise<any> {
        return super.onPageHiding();
    }
}