const {ccclass, property} = cc._decorator;

/**
 * 自定义的Item显示控制类.按需求随意设计
 */
@ccclass
export default class ListItem extends cc.Component {

    @property(cc.Label)
    private label: cc.Label = null;

    public setData(data: string) {
        console.log("....data", data);
        this.label.string = data;
    }

    public unuse() {
        cc.log("unuse...")
    }

    public reuse() {
        cc.log("reuse...")
    }
}
