// 声明全局开关，用于控制多点触摸是否允许，默认禁止多点触摸。
cc.view.multiTouchEnable = false;

cc.Node.maxTouchNum = 1;
cc.Node.touchNum = 0;

// 节点触摸操作的时间戳.
cc.Node.lastTouched = 0;

var __dispatchEvent__ = cc.Node.prototype.dispatchEvent;

cc.Node.prototype.dispatchEvent = function (event) {
    if (cc.view.multiTouchEnable) {
        __dispatchEvent__.call(this, event);
        return;
    }
    switch (event.type) {
        case 'touchstart':
            if (cc.Node.touchNum < cc.Node.maxTouchNum) {
                cc.Node.touchNum++;
                cc.Node.touchNum = cc.Node.touchNum > 1 ? 1 : cc.Node.touchNum;
                this._canTouch = true;
                __dispatchEvent__.call(this, event);
            }
            break;
        case 'touchmove':
            if (!this._canTouch && cc.Node.touchNum < cc.Node.maxTouchNum) {
                this._canTouch = true;
                cc.Node.touchNum++;
                cc.Node.touchNum = cc.Node.touchNum > 1 ? 1 : cc.Node.touchNum
            }

            if (this._canTouch) {
                __dispatchEvent__.call(this, event);
            }

            break;
        case 'touchend':
            if (this._canTouch) {
                this._canTouch = false;
                cc.Node.touchNum--;
                cc.Node.touchNum = cc.Node.touchNum < 0 ? 0 : cc.Node.touchNum;
                __dispatchEvent__.call(this, event);
            }
            cc.Node.lastTouched = cc.sys.now();
            break;
        case 'touchcancel':
            if (this._canTouch) {
                this._canTouch = true;
                cc.Node.touchNum--;
                cc.Node.touchNum = cc.Node.touchNum < 0 ? 0 : cc.Node.touchNum;
                __dispatchEvent__.call(this, event);
            }
            cc.Node.lastTouched = cc.sys.now();
            break;
        default:
            __dispatchEvent__.call(this, event);
    }
};

var __onPostActivated__ = cc.Node.prototype._onPostActivated;
cc.Node.prototype._onPostActivated = function (active) {
    if (!cc.view.multiTouchEnable && !active && this._canTouch) {
        this._canTouch = false;
        cc.Node.touchNum--;
        cc.Node.touchNum = cc.Node.touchNum < 0 ? 0 : cc.Node.touchNum;
    }
    __onPostActivated__.call(this, active);
};

var __onPreDestroy__ = cc.Node.prototype._onPreDestroy;
cc.Node.prototype._onPreDestroy = function () {
    if (!cc.view.multiTouchEnable && this._canTouch) {
        this._canTouch = false;
        cc.Node.touchNum--;
        cc.Node.touchNum = cc.Node.touchNum < 0 ? 0 : cc.Node.touchNum;
    }
    __onPreDestroy__.call(this);
};

// 获取最后点击时间长度.
export function getLastTouchDur() {
    if (cc.Node.lastTouched <= 0) return 0;
    return cc.sys.now() - cc.Node.lastTouched;
}

export function resetLastTouch() {
    cc.Node.lastTouched = cc.sys.now();
}